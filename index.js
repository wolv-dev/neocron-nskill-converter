const fs = require("fs");
const path = require("path");
const readline = require('readline');

const armorRegex = new RegExp('(?<=\\+ armour)(.*)[^\\d+.]', 'gi');
const modifierRegex = new RegExp('(?!<=\\+ armour)(.*)[^\\d+.]', 'gi');
const valueRegex = new RegExp('(\\d+)', 'gi');

/**
 * @description Read files synchronously from a folder, with natural sorting
 * @param {String} dir Absolute path to directory
 * @returns {Object[]} List of object, each object represent a file
 * structured like so: `{ filepath, name, ext, stat }`
 */
function readFilesSync(dir) {
  const files = [];

  fs.readdirSync(dir).forEach((filename) => {
    const name = path.parse(filename).name;
    const content = path.parse(filename)
    const ext = path.parse(filename).ext;
    const filepath = path.resolve(dir, filename);
    const stat = fs.statSync(filepath);
    const isFile = stat.isFile();

    if (isFile) files.push({ filepath, name, ext, stat });
  });

  files.sort((a, b) => {
    // natural sort alphanumeric strings
    // https://stackoverflow.com/a/38641281
    return a.name.localeCompare(b.name, undefined, {
      numeric: true,
      sensitivity: "base",
    });
  });

  return files;
}

const skills = {
  riflecombat: 'r-c',
  pistolcombat: 'p-c',
  techcombat: 't-c',
  heavycombat: 'h-c',
  weaponlore: 'wep',
  willpower: 'wpw',
  transport: 'tra',
  psiuse: 'psu',
  piercing: 'pcr',
  construction: 'cst',
  research: 'res',
  implant: 'imp',
  focussing: 'fcs',
  meleecombat: 'm-c',
  athletics: 'atl',
  agility: 'agl',
  endurance: 'end',
  remotecontrol: 'rcl',
  repair: 'rep',
  recycle: 'rec',
  barter: 'brt',
  force: 'for',
  pierce: 'pcr',
  energy: 'enr',
  fire: 'fir',
  xray: 'xrr',
  fcs: 'fcs',
  poison: 'por',
  health: 'hlt',
  dex: 'dex',
  str: 'str',
  int: 'int',
  psi: 'psi',
  con: 'con',
  apu: 'apu',
  ppu: 'ppu',
  hacking: 'hck'
}

// READ ARMOR FILES

const directory = path.join(__dirname, "./assets/psi/buffs");

const files = readFilesSync(directory);
files.forEach((file) => {
    let finalItemArrayArmor = [];
    var text = fs.readFileSync(file.filepath,'utf8');
    console.log(text);
    const readInterface = readline.createInterface({
        input: fs.createReadStream(file.filepath),
        output: process.stdout,
        console: false
    });
    let armorObject = { requirements: [], modifiers: []};
    let lineHasStarted = false;
    let lineIndex = 0;
    readInterface.on('line', function(line) {
        if (!line.includes('Holographic') && line.includes('graphic')) {
          return;
        }
        if (line.includes('duration') || line.startsWith('psi')) {
          return;
        }
        if (line.length  > 0) { lineIndex += 1; };
        console.log(line);
        if (line === 'start') {
            lineHasStarted = true;
            armorObject.type = file.name;
            return;
        }

        if(line.includes('Holographic')) {
          console.log('HERE');
        }
        if (lineHasStarted === true) {
            if (lineIndex === 2) {
                armorObject.name = line;
                return;
            }
            if (lineIndex >= 3) {
              if (line.startsWith('req')) {
                if (file.name === 'armor' || file.name.includes('buffs')) {
                  armorObject.tl = parseInt(line.substring(8, line.length));
                  armorObject.requirements.push({
                    
                    shortName: line.substring(4,7),
                    value: parseInt(line.substring(8, line.length))
                  });
                  return;
                }
              }
              if (file.name ==='drugs') {
                lineIndex = 4;
              }
          }
          if (lineIndex > 3 && line !== 'end') {
            if (line.includes('armour')) {
              const found = armorRegex.exec(line);
              const value = valueRegex.exec(line);
              if (found[0]) {
                armorObject.modifiers.push({
                  shortName: skills[found[0].trim()],
                  value: parseInt(value[0]),
                  type: 'armor'
                });
              }
              armorRegex.lastIndex = 0;
              valueRegex.lastIndex = 0;
              return;
            } else {
              const found = modifierRegex.exec(line);
              const value = valueRegex.exec(line);
              if (found && found[0]) {
                absValue = found[0].charAt(0) === '-' ? parseInt(value[0]) * -1 : parseInt(value[0]);
                armorObject.modifiers.push({
                  shortName: skills[found[0].substring(1).trim()] || 'BULLSHIT',
                  value: absValue
                });
              }
              modifierRegex.lastIndex = 0;
              valueRegex.lastIndex = 0;
              return;
            }
          }
        }
        if (line === 'end') {
            finalItemArrayArmor.push(armorObject);
            armorObject = { requirements: [], modifiers: []};
            lineHasStarted = false;
            lineIndex = 0;
        }
    });

    readInterface.on('close', function (line) {
      fs.writeFileSync(finalItemArrayArmor[0].type +'.json', JSON.stringify(finalItemArrayArmor));
    });


});


